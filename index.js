const express = require ('express')
const app = express()
var port = process.env.PORT || 8080
app.use (express.urlencoded({extended:false}))
const cors = require('cors')
app.use(cors())
app.listen(port, () =>
    console.log('HTTP Server with Express.js is listening on port: '+ port));

app.get('/', (req,res) => {
    res.send('Microservice 1 Gateway by Dineshchoudhari Yarlagadda and Venkata Krishna Prasanth Budigi. Usage: host/roman_binary?number=x')
})
app.get('/roman_binary', function (req,res){
    var number = req.query.number
    if (!Number.isInteger(Number(number)))
        return res.send("I cannot identify your number")
    const rnum = ["No Roman Number","I","II","III","IV","V","VI","VII","VIII","IX"]
    const bnum = ["0000","0001","0010","0011","0100","0101","0110","0111","1000","1001"]
    var romannum = rnum [number%10]
    var binarynum = bnum[number%10]
    res.send('The roman number is ' + romannum +', its binary number is '+ binarynum)
});

